package co.simplon.springcrud.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.springcrud.entity.Dog;

@Repository
public class DogDao implements IDao<Dog> {
    private static final String GET_DOGS = "SELECT * FROM dog";
    private static final String ADD_DOG = "INSERT INTO dog (name, breed, created_at) VALUES (?,?,?)";
    private static final String GET_DOG_BY_ID = "SELECT * FROM dog WHERE id=?";
    // private static final String UPDATE_DOG_BY_ID = "UPDATE dog SET name=?, breed=?, created_at=?";
    // private static final String DELETE_DOG_BY_ID = "DELETE FROM dog WHERE id=?";

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Dog> findAll() {
        Connection cnx;
        List<Dog> list = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();

            PreparedStatement stmt = cnx.prepareStatement(GET_DOGS);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(new Dog(rs.getInt("id"), rs.getString("name"), rs.getString("breed"),
                        rs.getDate("created_at")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public int add(Dog dog) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(ADD_DOG, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, dog.getName());
            stmt.setString(2, dog.getBreed());
            stmt.setDate(3, dog.getCreatedAt());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                dog.setId(rs.getInt(1));
            }
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dog.getId();
    }

    public Dog getDogById(Integer dogId) {
        Connection cnx;
        Dog dog = null;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_DOG_BY_ID);
            stmt.setInt(1, dogId);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String breed = rs.getString("breed");
                Date createdAt = rs.getDate("created_at");

                dog = new Dog();
                dog.setId(id);
                dog.setName(name);
                dog.setBreed(breed);
                dog.setCreatedAt(createdAt);
            }
            cnx.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dog;
    }

}
