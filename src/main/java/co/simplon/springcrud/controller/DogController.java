package co.simplon.springcrud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import co.simplon.springcrud.dao.DogDao;
import co.simplon.springcrud.entity.Dog;

@Controller
public class DogController {
    @Autowired
    private DogDao dogDao;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("dogs", dogDao.findAll());
        model.addAttribute("newDog", new Dog());
        return "index";
    }

    @GetMapping("/new")
    public String getAddPage(Model model) {
        Dog dog = new Dog();
        model.addAttribute("dog", dog);
        return "add";
    }

    @PostMapping("/add")
    public String addDog(@ModelAttribute("dog") Dog dog, Model model) {
        dogDao.add(dog);
        return "redirect:/";
    }

    @GetMapping("/get/{id}")
	public ModelAndView getSubscriberById(@PathVariable(name = "id") int id) {
	    ModelAndView mav = new ModelAndView("get");
	    Dog dog = dogDao.getDogById(id);
	    mav.addObject("dog", dog);
	    return mav;
	}
}